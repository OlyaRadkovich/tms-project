import allure

from src.base_element import BaseElement
from src.constants import Host
from src.main_page import MainPage


@allure.suite("Smoke")
@allure.story("Checking main page elements")
class TestMainPageItems:

    @allure.title("Opening the main page")
    def test_open_main_page(self, driver):
        with allure.step("Kinakipa's home page is opened"):
            assert driver.current_url == Host.KIPA_MAIN

    # Проверяем содержимое хэдера и кнопки меню
    @allure.title("Header testing")
    def test_header_items(self, driver):

        with allure.step("Check, that header contains the logo and menu buttons"):
            mp = MainPage(driver)
            header_item = [mp.header_menu, mp.logo, mp.movie_button,
                           mp.serial_button, mp.anime_button,
                           mp.donation_button, mp.sign_in_button]

            for item in header_item:
                item.assert_element_on_page()

        with allure.step("Click on the buttons 'фільмы', 'серыялы', 'мультфільмы', 'падтрымаць', 'увайсці'."):
            menu_item = [mp.movie_button, mp.serial_button,
                         mp.anime_button, mp.donation_button,
                         mp.sign_in_button]
            urls = ['https://kinakipa.site/Catalog?type=movie',
                    'https://kinakipa.site/Catalog?type=tv',
                    'https://kinakipa.site/Catalog?genre=%D0%90%D0%BD%D1%96%D0%BC%D1%8D',
                    'https://www.patreon.com/kinakipa',
                    'https://www.patreon.com/kinakipa']

            for item, url in zip(menu_item, urls):
                with allure.step(f"Page {url} is opened"):
                    try:
                        item.cool_click()
                        driver.switch_to.window(driver.window_handles[-1])
                        assert driver.current_url == url
                        driver.switch_to.window(driver.window_handles[0])
                    except (AssertionError):
                        with allure.step(f"Page {url} is currently not available"):
                            print(f"Page {url} is currently not available")

    # Проверяем ссылки в футере и кнопку-стрелку "вверх"
    @allure.title("Footer testing")
    def test_footer_items(self, driver):
        mp = MainPage(driver)
        mp.footer.scroll_to_end()

        with allure.step("Click on the links in the footer: VK page, Telegram, bot, Patreon"):
            footer_item = [mp.vk_link, mp.tg_link, mp.patreon_link, mp.bot_link]
            urls = ['https://vk.com/kinakipa', 'https://t.me/kinakipaby',
                    'https://www.patreon.com/kinakipa', 'https://t.me/kinakipa_site_bot']

            for item, url in zip(footer_item, urls):
                item.assert_element()
                item.cool_click()
                with allure.step(f"Page {url} is opened"):
                    try:
                        if item != mp.bot_link:
                            item.cool_click()
                            driver.switch_to.window(driver.window_handles[-1])
                            assert driver.current_url == url
                            driver.switch_to.window(driver.window_handles[0])
                        else:
                            assert driver.current_url == url
                            driver.back()
                    except (ConnectionError):
                        with allure.step(f"Page {url} is currently not available"):
                            print(f"Page {url} is currently not available")

        with allure.step("Click to the 'up' button"):
            mp.up_button.cool_click()
            with allure.step("Back to the header"):
                mp.logo.assert_element()

    # Проверяем, присутствуют ли на странице разделы с рекомендациями фильмов
    @allure.title("Testing sections with recommendations")
    def test_recommendations(self, driver):
        mp = MainPage(driver)
        for i in range(1, 7):
            chapter_xpath = f'{mp.recommendations_xpath}[{i}]'
            chapter = BaseElement(driver, chapter_xpath)
            with allure.step(f"Section {i} is displayed"):
                chapter.assert_element()

    # Проверяем работу кнопки "Дадому"
    @allure.title("Testing 'Back home' button")
    def test_back_home(self, driver):
        mp = MainPage(driver)
        movie = mp.catalog_xpath.get_random_by_xpath(driver)
        with allure.step("Go to the page of a random movie"):
            movie.cool_click()
        with allure.step("On the movie page click on the 'Back home' button"):
            mp.back_home_button.click()
            with allure.step("Return to the main page"):
                assert driver.current_url == Host.KIPA_MAIN

    # Проверяем, что клик на логотип возвращает на главную страницу
    @allure.title("Testing 'Logo' button")
    def test_logo_back_home(self, driver):
        mp = MainPage(driver)
        movie = mp.catalog_xpath.get_random_by_xpath(driver)
        with allure.step("Go to the page of a random movie"):
            movie.cool_click()
        with allure.step("On the movie page click on the logo"):
            mp.logo.click()
            with allure.step("Return to the main page"):
                assert driver.current_url == Host.KIPA_MAIN
