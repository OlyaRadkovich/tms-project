import allure
import pytest
from selenium.common import NoSuchElementException

from src.constants import Host
from src.main_page import MainPage
from src.movie_page import MoviePage


# Функция передаёт в строку поиска слово, переходит на страницу с результатами,
# далее переходит на страницу одного из фильмов и проверяет, содержится ли данное
# слово в соответствующем разделе информации о фильме. Если вводится беспорядочное сочетание
# букв, функция вызывает исключение, т.к. сайт открывает страницу без контента
def search_and_check(driver, word, element):
    mp = MainPage(driver)
    text_content = []

    try:
        mp.search_button.click()
        mp.search_field.send_keys(word)
        mp.search_submit.click()
        mp.movies_xpath.get_random_by_xpath(driver).cool_click()
        text_content = element.get_text().lower()
        with allure.step(f"The name/description of the suggested movie contains {word}"):
            assert word.lower() in text_content
    except AssertionError:
        with allure.step("The search returns irrelevant results. An AssertionError occurred"):
            print(f'Sometimes a search for a "{word}" will show something like "{text_content}".')
        pass
    except NoSuchElementException:
        with allure.step("NoSuchElementException"):
            print(f'Search for the "{word}" did not return any results.')


@allure.suite("Smoke")
@allure.story("Testing the movie search function")
class TestSearch:
    # Проверяем поиск по названию/части названия
    @allure.title("Checking the search string")
    @allure.step("Search by name/part of a name from a given list")
    def test_input_word(self, driver):
        mvp = MoviePage(driver)
        for word in Host.WORDS:
            with allure.step(f"Serch by {word}"):
                search_and_check(driver, word, mvp.movie_title)

    # Проверяем поиск по имени режиссёра или актёра (в данном случае поиск возможен только на кириллице)
    @allure.title("Checking the search string")
    @allure.step("Search by director/actor name from a given list")
    def test_input_name(self, driver):
        mvp = MoviePage(driver)
        for name in Host.NAMES:
            with allure.step(f"Serch by {name}"):
                search_and_check(driver, name, mvp.movie_about)

    # Кликаем 5 раз на кнопку "Мне пашанцуе!" и проверяем, что каждый раз открывается новая страница
    @allure.title("Checking a random search through the 'Мне пашанцуе!' button")
    def test_random_search(self, driver):
        with allure.step("Click on the button 5 times"):
            mp = MainPage(driver)
            url_list = []
            for _ in range(5):
                mp.lucky_me_button.click()
                driver.switch_to.window(driver.window_handles[-1])
                url = driver.current_url
                url_list.append(url)
                driver.back()
            url_set = set(url_list)
            with allure.step("Every time the site offers a new film"):
                assert len(url_list) == len(url_set)

    # Поиск по одному параметру
    @allure.title("Checking search by one parameter")
    @allure.step("Randomly select one parameter to search for")
    def test_search_with_one_filter(self, driver):
        mp = MainPage(driver)
        mvp = MoviePage(driver)

        main_parameter = mp.any_filter_button.get_random_by_xpath(driver)
        main_parameter.click()
        mp.filter_field.hover()
        filter = mp.filter_list_xpath.get_random_by_xpath(driver)
        filter_name = filter.get_text().lower()
        with allure.step(f"Search for {filter_name}"):
            filter.cool_click()
            mp.submit_filter_button.click()
            mp.movies_xpath.get_random_by_xpath(driver).cool_click()
            movie_info = mvp.movie_about.get_text().lower()
            if filter_name not in ["серыялы", "фільмы", "папулярнасць imdb ⬆",
                                   "рэйтынг imdb ⬆", "дата рэдагавання ⬆",
                                   "агучка і субцітры", "субцітры", "галасы кп ⬆",
                                   "год ⬆", "прэм'ера ⬆"]:
                with allure.step(f"The suggested film corresponds to the '{filter_name}' parameter"):
                    assert filter_name in movie_info
            else:
                with allure.step(f"Sorry, search checking for '{filter_name}' is not configured yet"):
                    pytest.skip(f"Skipping assert for '{filter_name}'.")


    # Поиск по двум параметрам: страна и жанр
    @allure.title("Checking search by two parameters")
    @allure.step("Checking the search using country and genre")
    def test_search_with_filters(self, driver):
        mp = MainPage(driver)
        mvp = MoviePage(driver)
        filter_name = []

        # Функция принимает список значений параметра, выбирает в нем случайный элемент,
        # кликает по этому элементу и передает его текстовое значение в список
        def choose(element):
            filter = mp.filter_list_xpath.get_random_by_xpath(driver)
            filter_name.append(filter.get_text().lower())
            with allure.step(f"Select {filter_name}"):
                filter.cool_click()

        with allure.step("Selected a random genre"):
            choose(mp.search_by_genre.get_random_by_xpath(driver).click())
        with allure.step("Add a random country"):
            choose(mp.search_by_country.get_random_by_xpath(driver).click())
        mp.submit_filter_button.click()

        # Выясняем, существуют ли на сайте фильмы с такими параметрами
        full_result = mvp.search_result.get_text()
        # Если существуют, проверяем соответствие одного из них заданным параметрам
        try:
            if len(full_result) > 9:
                mp.movies_xpath.get_random_by_xpath(driver).cool_click()
                about = mvp.movie_about.get_text().lower()
                with allure.step("The site offered movies according to the parameters"):
                    for name in filter_name:
                        assert name in about
            else:
                raise NoSuchElementException
        except(NoSuchElementException):
            with allure.step("There are no such films on the site"):
             print(f"There aren't any films with parameters {filter_name} on the site")
