import allure

from src.main_page import MainPage
from src.movie_page import MoviePage


@allure.suite("Smoke")
@allure.story("Checking movie page elements")
class TestMoviePage:
    # Проверяем элементы на странице фильма
    @allure.title("Cheking the presence of elements on the page")
    def test_elements_on_page(self, driver):
        mp = MainPage(driver)
        mvp = MoviePage(driver)
        movie = mp.catalog_xpath.get_random_by_xpath(driver)
        with allure.step("Go to the page of a random movie"):
            movie.cool_click()
            with allure.step("The title, the poster, the description and the player are on the page"):
                for item in [mvp.movie_title, mvp.movie_poster,
                             mvp.movie_about, mvp.movie_description,
                             mvp.movie_player]:
                    item.assert_element_on_page()

    # Выборочно проверяем работу ссылок на странице
    @allure.title("Random testing of recommendations on the movie page")
    def test_open_recommendations(self, driver):
        mp = MainPage(driver)
        mvp = MoviePage(driver)
        mp.search_button.click()
        with allure.step("Type 'rick and morty' in the search bar"):
            mp.search_field.send_keys('rick and morty')
        with allure.step("Go to the movie page"):
            mp.search_submit.click()
            mvp.rick_morty.click()
        with allure.step("Click on the word 'Фантастыка' in description"):
            mvp.genre_fantastic.cool_click()
        with allure.step("Go to the page with recommended films"):
            assert 'Фантастыка' in mvp.search_result_title.get_text()
            with allure.step("Opened page contains films in this genre"):
                mp.movies_xpath.get_random_by_xpath(driver).cool_click()
                assert 'фантастыка' in mvp.movie_about.get_text()
