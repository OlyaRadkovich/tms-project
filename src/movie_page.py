from src.base_element import BaseElement
from src.base_page import BasePage


class MoviePage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)


        self.movie_title = BaseElement(driver, '//*[@class="about-serial-header__subtitle"]')
        self.movie_about = BaseElement(driver, '//*[@class ="about-serial-right"]')
        self.movie_poster = BaseElement(driver, '//*[@class="about-serial-poster"]')
        self.movie_description = BaseElement(driver, '//*[@class="serial-description"]')
        self.movie_player = BaseElement(driver, '//*[@id="cinemaplayer-iframe"]')
        self.search_result_title = BaseElement(driver, '/html/body/div[1]/main/div/div/div/div/nav/ul/li[2]')
        self.search_result = BaseElement(driver, '/html/body/div[1]/main/div/div/div/div/div[2]')
        self.rick_morty = BaseElement(driver, '//a[@title="Рык і Морці"]')
        self.genre_fantastic = BaseElement(driver, '//a[contains(text(),"фантастыка")]')

