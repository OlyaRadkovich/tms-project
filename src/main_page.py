from src.base_element import BaseElement
from src.base_page import BasePage


class MainPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

        # ======================= Elements of header ==================================

        self.header = BaseElement(driver, '//*[@id="header"]')
        self.logo = BaseElement(driver, '//*[@class="header-logo"]')
        self.header_menu = BaseElement(driver, '//*[@class="navigation-links"]')
        self.search_button = BaseElement(driver, '//*[@class="btn__search"]')
        self.search_field = BaseElement(driver, '//*[@id="search-top-field"]')
        self.search_submit = BaseElement(driver, '//*[@class="header-search__button-submit"]')
        self.movie_button = BaseElement(driver, '//*[@href="/Catalog?type=movie"]')
        self.serial_button = BaseElement(driver, '//*[@href="/Catalog?type=tv"]')
        self.anime_button = BaseElement(driver, '//*[@href="/Catalog?genre=Мультфільм"]')
        self.donation_button = BaseElement(driver, '//*[@class="navigation-links"]'
                                                   '/descendant::*[@href="https://www.patreon.com/kinakipa"]')
        self.sign_in_button = BaseElement(driver, '//*[contains (@href, "Account")]')

        # ======================= Elements of footer ==================================

        self.footer = BaseElement(driver, '//*[@class="footer"]')
        self.vk_link = BaseElement(driver, '//*[@href="https://vk.com/kinakipa"]')
        self.tg_link = BaseElement(driver, '//*[@href="https://t.me/kinakipaby"]')
        self.mail_link = BaseElement(driver, '//*[@href="mailto:kinakipasite@gmail.com"]')
        self.bot_link = BaseElement(driver, '//*[@href="https://t.me/kinakipa_site_bot"]')
        self.patreon_link = BaseElement(driver, '//*[@class="footer__contacts"]/'
                                                'descendant::*[@href="https://www.patreon.com/kinakipa"]')

        # ==============================================================================

        self.up_button = BaseElement(driver, '//*[@class="scroll-top-button visible"]')
        self.back_home_button = BaseElement(driver, '//*[@class="breadcrumbs"]/descendant::*[@href="/"]')

        self.recommendations_xpath = '//*[@class="last-episodes-day__title"]'

        self.catalog_xpath = BaseElement(driver, '//*[@class="last-episodes-day"]/descendant:: a')
        self.movies_xpath = BaseElement(driver, '//*[@class="catalog"]/descendant:: a')

        # мне пашанцуе
        self.lucky_me_button = BaseElement(driver, '//*[@id="random-filter"]')

        # ============================= Search parameters ===================================================

        self.submit_filter_button = BaseElement(driver, '//*[@id="submit-filter"]')
        self.reset_filter_button = BaseElement(driver, '//*[@id="reset-filter"]')

        self.any_filter_button = BaseElement(driver, '//*[@class="custom-select select select-init"]')

        # жанры
        self.search_by_genre = BaseElement(driver, '//*[@class="custom-select select select-init"][2]')
        self.filter_field = BaseElement(driver, '//*[@class="select-items"]')
        self.filter_list_xpath = BaseElement(driver, '//*[@class="select-items"]/child::*')

        # страны
        self.search_by_country = BaseElement(driver, '//*[@class="custom-select select select-init"][3]')
