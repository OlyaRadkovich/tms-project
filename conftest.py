import pytest
from selenium import webdriver

from src.constants import Host
from src.main_page import MainPage


@pytest.fixture
def driver():
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    with webdriver.Chrome(options) as driver:
        yield driver


@pytest.fixture(autouse=True)
def open_main_page(driver, request):
        if request.node.get_closest_marker("skip_main_page"):
            pytest.skip("Skipping main page for this test")
        else:
            main_page = MainPage(driver)
            main_page.driver.get(Host.KIPA_MAIN)
